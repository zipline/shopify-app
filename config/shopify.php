<?php

	return [
		'key'         => env('SHOPIFY_KEY'),
		'pass'        => env('SHOPIFY_PASS'),
		'url'         => env('SHOPIFY_URL'),     // The project's my-shopify url
		'location_id' => env('SHOPIFY_LOCATION') // The location id used for restocking items.
	];