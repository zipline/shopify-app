<?php

	namespace App\Services;

	class ShopifyService {
		/**
		 * Make an API request to shopify
		 *
		 * @param        $url
		 * @param array  $params
		 * @param string $method
		 *
		 * @return mixed
		 */
		public function makeRequest($url, $method = 'POST', $params = [])
		{
			$curl = curl_init();
			$curl_url = 'https://'.config('shopify.key').':'.config('shopify.pass').'@'.config('shopify.baseUrl').$url;

			curl_setopt_array($curl, array(
				CURLOPT_URL            => $curl_url,
				CURLOPT_HEADER         => 0,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_HTTPHEADER     => array('Content-Type:application/json'),
				CURLOPT_CUSTOMREQUEST  => $method,
				CURLOPT_POSTFIELDS     => $params
			));

			$response = curl_exec($curl);
			$err      = curl_error($curl);

			curl_close($curl);

			if ($err) {
				// Add your own error handling here.

				return false;
			} else {
				return json_decode($response);
			}
		}

		/**
		 * Retrieve a product by it's handle
		 *
		 * @param        $handle
		 *
		 * @return mixed
		 */
		public function getProductByHandle($handle)
		{
			return $this->makeRequest('/products/'.$handle.'.js', 'GET');
		}

		/**
		 * Retrieve a product by it's id
		 *
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function getProductById($id)
		{
			return $this->makeRequest('/admin/products/'.$id.'.json', 'GET');
		}

		/**
		 * Retrieve a single order from shopify
		 *
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function getOrder($id)
		{
			return $this->makeRequest('/admin/orders/'.$id.'.json', 'GET');
		}

		/**
		 * Get an order's order number from the longer id
		 *
		 * @param $orderId
		 *
		 * @return mixed
		 */
		public function getOrderNum($orderId)
		{
			$result = $this->getOrder($orderId);
			return @$result->order->order_number;
		}

		/**
		 * Get customer from shopify id
		 *
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function getCustomer($id)
		{
			return $this->makeRequest('/admin/customers/'.$id.'.json', 'GET');
		}

		/**
		 * Get addresses associated with a customer
		 *
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function getCustomerAddress($id)
		{
			return $this->makeRequest('/admin/customers/'.$id.'/addresses.json', 'GET');
		}

		/**
		 * Get open orders from Shopify
		 *
		 * @return mixed
		 */
		public function getNewOrders()
		{
			return $this->makeRequest('/admin/orders.json?status=open', 'GET');
		}

		/**
		 * Create a new order
		 *
		 * @param $order
		 *
		 * @return mixed
		 */
		public function createNewOrder($order)
		{
			return $this->makeRequest('/admin/orders.json', 'POST', $order);
		}

		/**
		 * Update a product
		 *
		 * @param        $productId
		 * @param        $data
		 *
		 * @return mixed
		 */
		public function updateProduct($productId, $data)
		{
			return $this->makeRequest('/admin/products/'.$productId.'.json', 'PUT', $data);
		}

		/**
		 * Create product
		 *
		 * @param        $data
		 *
		 * @return mixed
		 */
		public function createProduct($data)
		{
			return $this->makeRequest('/admin/products.json', 'POST', $data);
		}

		/**
		 * @param        $productId
		 * @param        $productVariant
		 *
		 * @return mixed
		 */
		public function getVariant($productId, $productVariant)
		{
			return $this->makeRequest('/admin/products/'.$productId.'/variants/'.$productVariant.'.json', 'GET');
		}

		/**
		 * Get the variants from a product ID
		 *
		 * @param  $productId
		 *
		 * @return mixed
		 */
		public function getVariants($productId)
		{
			return $this->makeRequest('/admin/products/'.$productId.'/variants.json', 'GET');
		}

		/**
		 * Count the number of products on the site.
		 * This is mostly used for the getAllProducts method to determine how many pages there are.
		 *
		 * @return mixed
		 */
		public function getProductCount()
		{
			return $this->makeRequest('/admin/products/count.json', 'GET');
		}

		/**
		 * Get all products, this is a paginated response so cycle through all
		 * pages using the getProductCount method to get every item
		 *
		 * @param int    $page
		 * @param int    $limit
		 *
		 * @return mixed
		 */
		public function getAllProducts($page = 1, $limit = 250)
		{
			return $this->makeRequest('/admin/products.json?limit=' . $limit . '&page=' . $page, 'GET');
		}

		/**
		 * Update an item's available quantity
		 * You need to know the shop's location id and the product's inventory id.
		 *
		 * @param $inventoryId
		 * @param $newQty
		 *
		 * @return mixed
		 */
		public function updateQuantity($inventoryId, $newQty)
		{
			$data = [
				'location_id'       => config('shopify.location_id'),
				'inventory_item_id' => $inventoryId,
				'available'         => $newQty
			];

			return $this->makeRequest('/admin/inventory_levels/adjust.json', 'POST', $data);
		}

		/**
		 * Add an image to shopify
		 *
		 * @param $productId
		 * @param $data
		 *
		 * @return mixed
		 */
		public function addImage($productId, $data)
		{
			return $this->makeRequest('/admin/products/'.$productId.'/images.json', 'POST', $data);
		}

		/**
		 * Get a product's image
		 *
		 * @param $productId
		 *
		 * @return mixed
		 */
		public function getImages($productId)
		{
			return $this->makeRequest('/admin/products/'.$productId.'/images.json', 'GET');
		}

		/**
		 * Create a fulfillment
		 *
		 * @param        $orderId
		 * @param        $json
		 *
		 * @return mixed
		 */
		public function makeFulfillment($orderId, $json)
		{
			return $this->makeRequest('/admin/orders/'.$orderId.'/fulfillments.json','POST', $json);
		}

		/**
		 * Get user metafields
		 *
		 * @param $userId
		 *
		 * @return mixed
		 */
		public function userMetafields($userId)
		{
			return $this->makeRequest('/admin/customers/'.$userId.'/metafields.json', 'GET');
		}

		/**
		 * Get transactions for a single order
		 *
		 * @param $orderId
		 *
		 * @return mixed
		 */
		public function getTransactions($orderId)
		{
			return $this->makeRequest('/admin/api/2019-04/orders/'.$orderId.'/transactions.json', 'GET');
		}

		/**
		 * Get Shopify collects
		 *
		 * @return mixed
		 */
		public function getCollects()
		{
			return $this->makeRequest('/admin/api/2019-04/collects.json', 'GET');
		}
	}